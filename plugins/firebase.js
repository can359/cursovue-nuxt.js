import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/firestore')
require('firebase/storage')

const firebaseConfig = {
  apiKey: 'AIzaSyDjZG3CfsopgGuuTG1N0Llz8p9Rix0RqGk',
  authDomain: 'nuxt-1-38d19.firebaseapp.com',
  projectId: 'nuxt-1-38d19',
  storageBucket: 'nuxt-1-38d19.appspot.com',
  messagingSenderId: '724816171228',
  appId: '1:724816171228:web:a890f6640871fe14c7a419'
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

export { firebase, db, auth, storage }
